﻿using System;

namespace master.Endpoint
{
    public class TcpServer
    {
        private SessionRepository _sessionRepository;

        public TcpServer(SessionRepository sessionRepository)
        {
            _sessionRepository = sessionRepository;
            _sessionRepository.SessionChanged += SessionRepositoryOnSessionChanged;
        }

        private void SessionRepositoryOnSessionChanged(Session session, SessionEventArgs e)
        {
            Console.WriteLine("hey from Tcp");
        }
    }
}