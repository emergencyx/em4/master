using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace master
{
    class HttpServer
    {
        private Task _webserverTask;
        private SessionRepository _sessionRepository;

        private readonly Collection<WebSocket> _webSockets;

        public HttpServer(SessionRepository sessionRepository)
        {
            _webSockets = new Collection<WebSocket>();

            _sessionRepository = sessionRepository;
            _sessionRepository.SessionChanged += SessionRepositoryOnSessionChanged;

            AppContext.SetSwitch("Switch.Microsoft.AspNetCore.Server.Kestrel.Experimental.Http2", isEnabled: true);

            _webserverTask = (new WebHostBuilder())
                .UseStartup<ServerHandler>()
                .UseKestrel(options => { options.Listen(IPAddress.Loopback, 8888); })
                .ConfigureServices(service => { service.AddSingleton(typeof(HttpServer), this); })
                .Build()
                .RunAsync();
        }

        private async void SessionRepositoryOnSessionChanged(Session session, SessionEventArgs e)
        {
            var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(session));
            foreach (var webSocket in _webSockets)
            {
                await webSocket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }

        private async Task HandleRequest(HttpContext httpContext)
        {
            httpContext.Response.ContentType = "application/json";
            await httpContext.Response.WriteAsync(_sessionRepository.GetJson());
        }

        private async Task Echo(HttpContext context, WebSocket webSocket)
        {
            var buffer = new byte[1024];

            _webSockets.Add(webSocket);

            while (true)
            {
                var webSocketReceiveResult = await webSocket.ReceiveAsync(buffer, CancellationToken.None);
                if (!webSocketReceiveResult.CloseStatus.HasValue) continue;

                _webSockets.Remove(webSocket);
                break;
            }

            await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "See you later", CancellationToken.None);
        }

        private class ServerHandler
        {
            public void Configure(IApplicationBuilder app, HttpServer httpServer)
            {
                app.UseWebSockets(new WebSocketOptions());

                app.Use(async (context, next) =>
                {
                    if (context.Request.Path == "/ws")
                    {
                        if (context.WebSockets.IsWebSocketRequest)
                        {
                            var webSocket = await context.WebSockets.AcceptWebSocketAsync();
                            await httpServer.Echo(context, webSocket);
                        }
                        else
                        {
                            context.Response.StatusCode = 400;
                        }
                    }
                    else
                    {
                        await next();
                    }
                });

                app.Run(httpServer.HandleRequest);
            }
        }
    }
}