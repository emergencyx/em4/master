﻿using System;
using Newtonsoft.Json;

namespace master
{
    [JsonObject]
    public class Session
    {
        public string Id { get; set; }
        public int GameMode { get; set; }
        public bool Inet { get; set; }
        public int NumPl { get; set; }
        public int MaxPl { get; set; }
        public string Nation { get; set; }
        public bool Password { get; set; }
        public string[] Players { get; set; }
        public string ServerAddr { get; set; }
        public string ServerName { get; set; }
        public int ServerPort { get; set; }
        public short Vmaj { get; set; }
        public short Vmin { get; set; }
        public string Vtype { get; set; }
        public string Mod { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}


/*
 *      id=<uuidV4>
 *      game_mode=3
 *      inet=1
 *      numpl=1
 *      maxpl=4
 *      nation=de
 *      password=0
 *      players=[]
 *      server_addr=xxx.xxx.xxx.xxx
 *      server_name=Bushton Mod V3
 *      server_port=58282
 *      vmaj=1
 *      vmin=0
 *      vtype=f
 *      mod=Bushton Mod V3
 *      updated_at=<moment>
 *      created_at=<moment>
 */