﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace master
{
    public class SessionRepository
    {
        private Collection<Session> Sessions;

        public event SessionEventHandler SessionChanged;

        public SessionRepository()
        {
            Sessions = new Collection<Session>();
        }

        public void AddSession(Session session)
        {
            SessionChanged?.Invoke(session, new SessionEventArgs(SessionEventArgs.SessionEventType.Created));
            Sessions.Add(session);
        }

        public string GetJson()
        {
            return JsonConvert.SerializeObject(Sessions);
        }
    }

    public delegate void SessionEventHandler(Session session, SessionEventArgs e);

    public class SessionEventArgs : EventArgs
    {
        public SessionEventArgs(SessionEventType eventType)
        {
            EventType = eventType;
        }

        public enum SessionEventType
        {
            Created,
            Updated,
            Deleted
        }

        public SessionEventType EventType { get; set; }
    }
}