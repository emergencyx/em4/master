﻿using System;
using master.Endpoint;

namespace master
{
    public class Master
    {
        private HttpServer _httpServer;
        private TcpServer _tcpServer;
        private UdpServer _udpServer;
        private SessionRepository _sessionRepository;

        public Master()
        {
            _sessionRepository = new SessionRepository();

            _udpServer = new UdpServer(_sessionRepository);
            _tcpServer = new TcpServer(_sessionRepository);
            _httpServer = new HttpServer(_sessionRepository);

            Console.WriteLine("wait");
            Console.ReadKey();

            _sessionRepository.AddSession(new Session()
            {
                Id = "hallo welt!"
            });


            /*
  
              for (var i = 0; i < 10; i++)
              {
                  Task.Factory.StartNew(async () =>
                  {
                      var buffer = new byte[1024];
                      while (true)
                      {
                          using (var socket = await UdpSocket.AcceptAsync())
                          {
                              var data = await socket.ReceiveAsync(buffer, SocketFlags.None);
  
                              var message = Encoding.UTF8.GetString(buffer, 0, data);
  
                              socket.Send(buf, 0, buf.Length, SocketFlags.None);
                          }
                      }
                  }, TaskCreationOptions.LongRunning);
              }
              */
        }
    }
}